import React, { useCallback, useState } from "react";
import RatingComponent from "../../Components/Ratings/index";
import image1 from "../../assets/images/image1.png";
import image2 from "../../assets/images/image2.png";
import image3 from "../../assets/images/image3.png";
import image4 from "../../assets/images/image4.png";

const RatingContainer = () => {
  const data = [
    {
      review:
        "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas. Tortor condimentum lacinia quis vel. Nibh praesent tristique magna sit amet.",
      name: "Naruto Uzumaki",
      position: "CEO, ABC Company",
      avatar: image1
    },
    {
      review:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas. Tortor condimentum lacinia quis vel. Nibh praesent tristique magna sit amet.",
      name: "ABCD EFG",
      position: "CEO, ABC Company",
      avatar: image2
    },
    {
      review:
        "consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas. Tortor condimentum lacinia quis vel. Nibh praesent tristique magna sit amet.",
      name: "HIJ KLM",
      position: "CEO, ABC Company",
      avatar: image3
    },
    {
      review:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Odio ut enim blandit volutpat maecenas. Tortor condimentum lacinia quis vel. Nibh praesent tristique magna sit amet.",
      name: "NOPQ RST",
      position: "CEO, ABC Company",
      avatar: image4
    }
  ];

  const [active, setActive] = useState(0);

  const ButtonClickHandler = useCallback(
    e => {
      if (e.target.value === "next") {
        if (active >= data.length - 1) {
          setActive(0);
          return;
        }
        setActive(active + 1);
      }
      if (e.target.value === "previous") {
        if (active <= 0) {
          setActive(data.length - 1);
          return;
        }
        setActive(active - 1);
      }
    },
    [active, data]
  );

  const changeImageHandler = useCallback(e => {
    setActive(Number(e.target.name));
  }, []);

  return (
    <RatingComponent
      changeImageHandler={changeImageHandler}
      ButtonClickHandler={ButtonClickHandler}
      active={active}
      data={data}
    />
  );
};

export default RatingContainer;
