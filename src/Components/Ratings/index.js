import React from "react";
import Container from "./styles/Container";
import TextContainer from "./styles/TextContainer";
import ImageContainer from "./styles/ImageContainer";
import MainImage from "./styles/MainImage";
import { Img } from "./styles/RotatingImage";
import ReviewSwitchContainer from "./styles/ReviewSwitchContainer";

const RatingComponent = ({
  changeImageHandler,
  ButtonClickHandler,
  active,
  data
}) => {
  return (
    <Container>
      <TextContainer>
        <div>
          <h1>What my Clients say?</h1>
          <p>{data[active].review}</p>
          <ReviewSwitchContainer>
            <div>
              <h4>{data[active].name}</h4>
              <h5>{data[active].position}</h5>
            </div>
            <div>
              <button value="previous" onClick={e => ButtonClickHandler(e)}>&#8592;</button>
              <button value="next" onClick={e => ButtonClickHandler(e)}>&#10230;</button>
            </div>
          </ReviewSwitchContainer>
        </div>
      </TextContainer>
      <ImageContainer>
        <MainImage src={data[active].avatar} alt="MainImage" />

        {data.map((review, index) => (
          <Img
            key={index}
            src={review.avatar}
            move={18 + index - 4}
            rotate={360 - (index + 2) * 53}
            name={index}
            onClick={e => {
              changeImageHandler(e);
            }}
          />
        ))}
      </ImageContainer>
    </Container>
  );
};

export default RatingComponent;
